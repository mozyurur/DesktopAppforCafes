const electron = require('electron');
const path = require('path');
const url = require('url');
const Sequelize = require('sequelize');
const Store = require('electron-store');

var unirest = require("unirest");
var req_signin = unirest("POST", "http://localhost:3002/signin");

var req_access_control = unirest("POST", "http://localhost:3002/user/access-control");

req_signin.headers({
  "Content-Type": "application/x-www-form-urlencoded"
});
req_access_control.headers({
  "Content-Type": "application/x-www-form-urlencoded"
});




// SET ENV
process.env.NODE_ENV = 'development';
process.env.ELECTRON_ENABLE_LOGGING = true;

const {app, BrowserWindow, Menu} = electron;
//set ipc to communicate between html pages and main page
const ipcMain = require('electron').ipcMain;

const store = new Store();



let mainWindow;



// Listen for app to be ready
app.on('ready', function(){

  // Create new window
  mainWindow = new BrowserWindow({});
  if (store.get('user-info.logedin')==true) {
    req_access_control.form({
      "username": store.get('user-info.username'),
      "password": store.get('user-info.password')
    });

    req_access_control.end(function (res) {
      console.log(res.body);
      if (res.error) {
        store.delete('user-info');
        mainWindow.loadURL(url.format({
          pathname: path.join(__dirname, 'signin.html'),
          protocol: 'file:',
          slashes:true
        }));
      }else {
        // Load html in window
        mainWindow.loadURL(url.format({
          pathname: path.join(__dirname, 'main.html'),
          protocol: 'file:',
          slashes:true
        }));
      }

    });

  }else{
    // Load html in window
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'signin.html'),
      protocol: 'file:',
      slashes:true
    }));
  }

  // Quit app when closed
  mainWindow.on('closed', function(){
    app.quit();
  });

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert menu
  Menu.setApplicationMenu(mainMenu);

});





// Create menu template
const mainMenuTemplate =  [
  // Each object is a dropdown
  {
    label: 'File',
    submenu:[
      {
        label: 'Quit',
        accelerator:process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click(){
          app.quit();
        }
      }
    ]
  }
];



// Add developer tools option if in dev
if(process.env.NODE_ENV !== 'production'){
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu:[
      {
        role: 'reload'
      },
      {
        label: 'Toggle DevTools',
        accelerator:process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
        click(item, focusedWindow){
          focusedWindow.toggleDevTools();
        }
      }
    ]
  });
}



ipcMain.on("connection_values",function(event,values){
  //set connection string to connect local mssql server
  // const sequelize = new Sequelize('mssql://sa:5945931999geS@BOZYURUR');
  const sequelize = new Sequelize('mssql://'+values.credentials[1].username+':'+values.credentials[2].password+'@'+values.credentials[0].servername);

  //authenticate to mssql server
  sequelize.authenticate().then(() => {
    addWindow.webContents.send("connection",true);
    store.set('database-info.isconnected', true);
    store.set('database-info.servername', values.credentials[0].servername);
    store.set('database-info.username', values.credentials[1].username);
    store.set('database-info.password', values.credentials[2].password);
    sequelize.query(`SELECT  [Id] FROM [SambaPOS3].[dbo].[MenuItems] WHERE [Name] = 'cafe-point'`, { type: sequelize.QueryTypes.SELECT}).then(menuitemid => {
     //send selected data to html page

     store.set('database-info.itemid', menuitemid[0].Id);
     event.sender.send('connection', true);
   });
  }).catch(err => {
  event.sender.send('connection', err);
  });
  addWindow.webContents.send("connection",values);


});

ipcMain.on("check_database_connection",function(event,values){
  if (store.get('database-info.isconnected')==true) {

    var saved_username = store.get('database-info.username');
    var saved_server_name = store.get('database-info.servername');
    var saved_password = store.get('database-info.password');

    //set connection string to connect local mssql server
    // const sequelize = new Sequelize('mssql://sa:5945931999geS@BOZYURUR');
    const sequelize = new Sequelize('mssql://'+saved_username+':'+saved_password+'@'+saved_server_name);

    //authenticate to mssql server
    sequelize.authenticate().then(() => {
       event.sender.send('connection', store.get('database-info.itemid'));
    }).catch(err => {
      event.sender.send('database_connection_checked', 'err');
    });
  }else {
    event.sender.send('database_connection_checked', false);
  }
});

ipcMain.on("add_discount",function(event,values){

  var saved_username = store.get('database-info.username');
  var saved_server_name = store.get('database-info.servername');
  var saved_password = store.get('database-info.password');

  //set connection string to connect local mssql server
  // const sequelize = new Sequelize('mssql://sa:5945931999geS@BOZYURUR');
  const sequelize = new Sequelize('mssql://'+saved_username+':'+saved_password+'@'+saved_server_name);

  //authenticate to mssql server
  sequelize.authenticate().then(() => {
    sequelize.query(`INSERT INTO [SambaPOS3].[dbo].[Orders]
             ([TicketId]
             ,[WarehouseId]
             ,[DepartmentId]
             ,[MenuItemId]
             ,[MenuItemName]
             ,[PortionName]
             ,[Price]
             ,[Quantity]
             ,[PortionCount]
             ,[Locked]
             ,[CalculatePrice]
             ,[DecreaseInventory]
             ,[IncreaseInventory]
             ,[OrderNumber]
             ,[CreatingUserName]
             ,[CreatedDateTime]
             ,[AccountTransactionTypeId]
             ,[OrderStates])
       VALUES
             (5
             ,1
             ,1
             ,45
             ,'cafe-point'
             ,'Normal'
             ,-3.000
             ,1.000
             ,1
             ,1
             ,1
             ,1
             ,0
             ,5
             ,'Cafe Point'
             ,'2018-06-05 21:23:20.193'
             ,3
             ,'[{"D":"\/Date(1528223002952+0300)\/","OK":"000000","S":"Submitted","SN":"Status","SV":"","U":1}]')`, { type: sequelize.QueryTypes.INSERT}).then(order => {


     event.sender.send('connection', true);
    });
  }).catch(err => {
    event.sender.send('database_connection_checked', 'err');
  });
});

ipcMain.on("get_open_tables",function(event,values){
  var saved_username = store.get('database-info.username');
  var saved_server_name = store.get('database-info.servername');
  var saved_password = store.get('database-info.password');

  //set connection string to connect local mssql server
  // const sequelize = new Sequelize('mssql://sa:5945931999geS@BOZYURUR');
  const sequelize = new Sequelize('mssql://'+saved_username+':'+saved_password+'@'+saved_server_name);

  //authenticate to mssql server
  sequelize.authenticate().then(() => {
    sequelize.query(`SELECT  [TicketNumber] FROM [SambaPOS3].[dbo].[Tickets] WHERE [IsClosed] = 0`, { type: sequelize.QueryTypes.SELECT}).then(tickets => {
     event.sender.send('connection', tickets);
   });
  }).catch(err => {
  event.sender.send('connection', err);
  });
});



ipcMain.on("open_connection_window",function(event,values){
  addWindow = new BrowserWindow({
    title:'Database Connection'
  });
  addWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'public/databaseConnection.html'),
    protocol: 'file:',
    slashes:true
  }));
  // Handle garbage collection
  addWindow.on('close', function(){
    addWindow = null;
  });
});

ipcMain.on("login_values",function(event,values){
  req_signin.form({
    "username": values.credentials[0].username,
    "password": values.credentials[1].password
  });

  req_signin.end(function (res) {
    if (res.body==true) {
      mainWindow.webContents.send("login",res.body);
      store.set('user-info.logedin', true);
      store.set('user-info.username', values.credentials[0].username);
      store.set('user-info.password', values.credentials[1].password);
      mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'main.html'),
        protocol: 'file:',
        slashes:true
      }));
    }else {
      mainWindow.webContents.send("login",res.body);
    }
  });

});

ipcMain.on("signout",function(event){
  store.delete('user-info');
  store.delete('database-info');
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'signin.html'),
    protocol: 'file:',
    slashes:true
  }));
});
